
    // var heigth_header = $("#content_header").outerHeight();
    // var heigth_slider = $(".content-slider").outerHeight();
    // var heigth_header_slider = heigth_header + heigth_slider;
    // $(".slide-contact,.content-slider").css({"top":heigth_header+"px"});
    // $(".content-body").css({"padding-top":heigth_header_slider+"px"});
    $("#tab1").click(function(){
    	$("#tabs-1").slideDown();
    	$("#tabs-2").slideUp();
    	$("#tabs-3").slideUp();
    	$(this).css({"color":"#FFF","border-bottom":"2px solid #FFF"});
    	$("#tab2").css({"color":"#022466","border-bottom":"2px solid transparent"});
    	$("#tab3").css({"color":"#022466","border-bottom":"2px solid transparent"});
    });
    $("#tab2").click(function(){
    	$("#tabs-1").slideUp();
    	$("#tabs-2").slideDown();
    	$("#tabs-3").slideUp();
    	$(this).css({"color":"#FFF","border-bottom":"2px solid #FFF"});
    	$("#tab1").css({"color":"#022466","border-bottom":"2px solid transparent"});
    	$("#tab3").css({"color":"#022466","border-bottom":"2px solid transparent"});
    });
    $("#tab3").click(function(){
    	$("#tabs-1").slideUp();
    	$("#tabs-2").slideUp();
    	$("#tabs-3").slideDown();
    	$(this).css({"color":"#FFF","border-bottom":"2px solid #FFF"});
    	$("#tab1").css({"color":"#022466","border-bottom":"2px solid transparent"});
    	$("#tab2").css({"color":"#022466","border-bottom":"2px solid transparent"});
    });

    $("#trust").click(function(){
    	$("#c_trust").slideToggle("fast");
    	$("#c_speed").slideUp();
    	$("#c_price").slideUp();
    	$("#c_quality").slideUp();
    	$("#c_frien").slideUp();
        var plus = $(".plus1").html();
        if(plus == "+"){
            signal = "-";
        }else{
            signal = "+"
        }
    	$(".plus1").html(signal);
    	$(".plus2").html("+");
    	$(".plus3").html("+");
    	$(".plus4").html("+");
    	$(".plus5").html("+");
    });
    $("#speed").click(function(){
    	$("#c_trust").slideUp();
    	$("#c_speed").slideToggle("fast");
    	$("#c_price").slideUp();
    	$("#c_quality").slideUp();
    	$("#c_frien").slideUp();
        var plus = $(".plus2").html();
        if(plus == "+"){
            signal = "-";
        }else{
            signal = "+"
        }
    	$(".plus1").html("+");
    	$(".plus2").html(signal);
    	$(".plus3").html("+");
    	$(".plus4").html("+");
    	$(".plus5").html("+");
    });
    $("#price").click(function(){
    	$("#c_trust").slideUp();
    	$("#c_speed").slideUp();
    	$("#c_price").slideToggle("fast");
    	$("#c_quality").slideUp();
    	$("#c_frien").slideUp();
        var plus = $(".plus3").html();
        if(plus == "+"){
            signal = "-";
        }else{
            signal = "+"
        }
    	$(".plus1").html("+");
    	$(".plus2").html("+");
    	$(".plus3").html(signal);
    	$(".plus4").html("+");
    	$(".plus5").html("+");
    });
    $("#quality").click(function(){
    	$("#c_trust").slideUp();
    	$("#c_speed").slideUp();
    	$("#c_price").slideUp();
    	$("#c_quality").slideToggle("fast");
    	$("#c_frien").slideUp();
        var plus = $(".plus4").html();
        if(plus == "+"){
            signal = "-";
        }else{
            signal = "+"
        }
    	$(".plus1").html("+");
    	$(".plus2").html("+");
    	$(".plus3").html("+");
    	$(".plus4").html(signal);
    	$(".plus5").html("+");
    });
    $("#frien").click(function(){
    	$("#c_trust").slideUp();
    	$("#c_speed").slideUp();
    	$("#c_price").slideUp();
    	$("#c_quality").slideUp();
    	$("#c_frien").slideToggle("fast");
        var plus = $(".plus5").html();
        if(plus == "+"){
            signal = "-";
        }else{
            signal = "+"
        }
    	$(".plus1").html("+");
    	$(".plus2").html("+");
    	$(".plus3").html("+");
    	$(".plus4").html("+");
    	$(".plus5").html(signal);
    });

    $(".show-contact").click(function(){
        $(".show-contact").css({"border-bottom":"2px solid #00AAEB","color":"#00AAEB"});
        $(".contact-popup").animate({"right":"0px"},"fast");
    });
    $(".contact-popup-close").click(function(){
        $(".show-contact").css({"border-bottom":"2px solid transparent","color":"#FFF"});
        $(".contact-popup").animate({"right":"-300px"},"fast");
    });

    $("#product_click").click(function (){
        $("#product_click").css({"border-bottom":"2px solid #00AAEB","color":"#00AAEB"});
        $('html, body').animate({
            scrollTop: $("#product").offset().top
        }, 500);
    });